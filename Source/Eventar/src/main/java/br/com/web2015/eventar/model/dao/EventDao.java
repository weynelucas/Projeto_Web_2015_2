package br.com.web2015.eventar.model.dao;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import br.com.web2015.eventar.model.entity.Event;
import br.com.web2015.eventar.model.entity.User;

@RequestScoped
public class EventDao {
	
	@Inject
	private Session session;
	
	private Criteria createCriteria(){
		return session.createCriteria(Event.class);
	}
	
	public void saveOrUpdate(Event event){
		if(event.getId() == null){
			session.save(event);
		} else{
			session.update(event);
		}
	}
	
	public void save(Event event){
		session.save(event);
	}
	
	public void delete(Event event){
		session.delete(event);
	}
	
	public void update(Event event){
		session.update(event);
	}
	
	@SuppressWarnings("unchecked")
	public List<Event> list(){
		return list(100, "publicationDate", false);
	}
	
	@SuppressWarnings("unchecked")
	public List<Event> list(int max){
		return list(max, "publicationDate", false);
	}
	
	@SuppressWarnings("unchecked")
	public List<Event> list(int max, String orderProperty, boolean isAsc){
		return createCriteria().setMaxResults(max).addOrder(isAsc ? Order.asc(orderProperty) : Order.desc(orderProperty)).list();
	}

	public Event find(Long id) {
		return (Event) session.load(Event.class, id);
	}
	
	@SuppressWarnings("unchecked")
	public List<Event> findByOwner(User user) {
		return createCriteria().add(Restrictions.eq("owner", user)).list();
	}
	
}
