package br.com.web2015.eventar.controller;

import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.observer.download.ByteArrayDownload;
import br.com.caelum.vraptor.observer.download.Download;
import br.com.caelum.vraptor.observer.upload.UploadedFile;
import br.com.web2015.eventar.annotation.Authenticate;
import br.com.web2015.eventar.component.UserSession;
import br.com.web2015.eventar.model.dao.EventDao;
import br.com.web2015.eventar.model.entity.Event;
import br.com.web2015.eventar.utils.ImageUtils;

@Controller
public class EventController {

	@Inject 
	private Result result;
	
	@Inject
	private HttpServletRequest request;
	
	@Inject
	UserSession userSession;
	
	@Inject
	private EventDao dao;
	
	@Authenticate
	public void form(){}
	
	@Authenticate
	public void save(Event event, UploadedFile image) {
		event.setOwner(userSession.getCurrentUser());
		
		if(event.getId()==null){
			event.setPublicationDate(new Date());
			result.include("message", "Event successfully registered");
		} else{
			result.include("message", "Event successfully edited");
		}
		
		if(image!=null){
			ImageUtils.uploadEventImage(image, event);	
		}
		
		dao.saveOrUpdate(event);
		
		
		result.redirectTo(this).my();
	}
	
	@Authenticate
	public void edit(Long id){
		result.include("event", dao.find(id));
		result.redirectTo(this).form();
	}
	
	@Authenticate
	public void delete(Long id){
		dao.delete(dao.find(id));
		result.include("message", "Event successfully deleted");
		result.redirectTo(this).my();
	}
	
	public Event show(Long id){
		return dao.find(id);
	}
	
	@Authenticate
	public List<Event> list(){
		return dao.list();
	}
	
	@Authenticate
	public List<Event> my(){
		result.include("my", true);
		return dao.findByOwner(userSession.getCurrentUser());
	}
	
	public Download image(Long id){
		Event event = dao.find(id);
		return new ByteArrayDownload(event.getImage(), "image/png", "Imagem");
	}
}
