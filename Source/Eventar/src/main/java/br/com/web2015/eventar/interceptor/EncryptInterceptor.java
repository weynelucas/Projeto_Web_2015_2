package br.com.web2015.eventar.interceptor;

import javax.inject.Inject;

import br.com.caelum.vraptor.AroundCall;
import br.com.caelum.vraptor.Intercepts;
import br.com.caelum.vraptor.core.MethodInfo;
import br.com.caelum.vraptor.interceptor.AcceptsWithAnnotations;
import br.com.caelum.vraptor.interceptor.SimpleInterceptorStack;
import br.com.web2015.eventar.annotation.EncryptPassword;
import br.com.web2015.eventar.model.entity.User;
import br.com.web2015.eventar.utils.EncryptUtils;

@Intercepts
@AcceptsWithAnnotations(EncryptPassword.class)
public class EncryptInterceptor {
	@Inject
	MethodInfo methodInfo;
	
	@AroundCall
	public void intercept(SimpleInterceptorStack stack){
		Object[] objects = methodInfo.getParametersValues();
		for(Object object : objects){
			if(object instanceof User){
				User user = (User) object;
				user.setPassword(EncryptUtils.encryt(user.getPassword()));
			}
		}
		stack.next();
	}
}
