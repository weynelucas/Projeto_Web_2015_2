<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<title><fmt:message key="event.list.my"/></title>

</head>
<body>
	<c:import url="/header.jsp"/>
	
	<div class="container">
		<c:set var="message" value="${message}"/>
	
		<h2 class="header orange-text lighten-1"><fmt:message key="event.list.my"/></h2>
	
		<div class="row">
			<c:import url="/cards.jsp"/>
		</div>	
	
	</div>
	
	<script type="text/javascript">
	$(document).ready(function(){
		if(message!=null){
			  Materialize.toast('${message}', 3000, 'rounded') 
		}
	})
	</script>
</body>
</html>