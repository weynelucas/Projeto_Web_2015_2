<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title><fmt:message key="app.name"/></title>
</head>
<body>
	<c:import url="/header.jsp"/>
	<c:set var="message" value="${message}"/>
	<div id="index-banner" style="background-image: url('http://www.dennisalberti.nl/wp-content/uploads/2015/05/concert.jpg');background-size: cover;background-position: center;position: relative;height: 610px;">

		<div class="title-container">
			<h1 class="white-text" style="margin: 0; position: absolute; top: 70%; left: 50%; margin-right: -50%; transform: translate(-50%,-50%)">
				<fmt:message key="app.name"/>
			</h1>
			<h6 class="white-text" style="margin: 0; position: absolute; top: 80%; left: 50%; margin-right: -50%; transform: translate(-50%,-50%)" >
				<fmt:message key="app.subtitle"/>
			</h6>
			<a class="orange lighten-1 waves-effect waves-light btn modal-trigger" href='<c:choose><c:when test="${userSession.isLoggedIn()}">${linkTo[EventController].form}</c:when><c:otherwise>#login-modal</c:otherwise></c:choose>' style="margin: 0; position: absolute; top: 90%; left: 50%; margin-right: -50%; transform: translate(-50%,-50%)"><fmt:message key="event.createYourEvent.label"/></a>
		</div>
	</div>
	
	<div class="container">
		<div class="row">
			<h1 class="header orange-text lighten-1"><fmt:message key="app.events"/></h1>
			<div id="events" class="section scrollspy">		
				<c:import url="/cards.jsp"/>
			</div>		
		</div>
		<a class="orange lighten-1 waves-effect waves-light btn modal-trigger" href='<c:choose><c:when test="${userSession.isLoggedIn()}">${linkTo[EventController].list}</c:when><c:otherwise>#login-modal</c:otherwise></c:choose>'><fmt:message key="app.seeMore"/></a>
	</div>

	
	<c:import url="/footer.jsp"/>
	
	<script type="text/javascript">
	$(document).ready(function(){
		if(message!=null){
			  Materialize.toast('${message}', 3000, 'rounded') 
		}
	})
	</script>
</body>
</html>