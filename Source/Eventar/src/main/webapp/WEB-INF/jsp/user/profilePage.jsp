<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title><fmt:message key="app.userProfile"/></title>
</head>
<body>

	<c:import url="/header.jsp"></c:import>
	
    <c:choose>
        <c:when test="${userSession.getCurrentUser().getUsername().equals("arthurbosousa@gmail.com")}">
			<a class="waves-effect waves-light orange lighten-1 white-text btn" style="margin-top:20px; margin-left:20px" href="${linkTo[UserController].list}"><fmt:message key="app.userManagement"/></a>
	    </c:when>
    </c:choose>
    
	<div class="container" style="padding-left:200px; height:430px">
	  <div class="row">
	  	<div class="col s12 m7">
	  	<h5 class="card-panel center orange lighten-1 white-text">${user.firstName} ${user.lastName}</h5>
	  	  <div class="card-panel">
			<div class="row"> 
		  	  	<div class="card-content">
		  	        <div class="col s6">
		  	            <c:choose>
        					<c:when test="${userSession.getCurrentUser().getUsername().equals("arthurbosousa@gmail.com") || userSession.getCurrentUser().getId() == user.id}">
						           	<a class="btn-floating btn-medium orange lighten-1 modal-trigger" href="${linkTo[UserController].edit}?id=${user.id}">
		      							<i class="medium material-icons">mode_edit</i>
		    						</a>
	    					</c:when>
    					</c:choose>
			            <img src="https://upload.wikimedia.org/wikipedia/commons/4/41/AWS_Simple_Icons_Non-Service_Specific_User.svg" style="width:200px; height:200px">
			        </div>
			        <div class="col s6">
	  					<h6 style="font-weight:bold"><fmt:message key="user.username"/>:</h6>
	  					<p style="text-align: center">${user.username}</p>
	  					<h6 style="font-weight:bold"><fmt:message key="user.additionalInfo"/>:</h6>
			        </div>
		        </div>
		  	  </div>
		  </div>
	  	</div>
	  </div>  
	</div>
	
</body>
</html>