<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>   
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/js/materialize.min.js"></script>
  	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/css/materialize.min.css">
  	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="<c:url value="/css/main.css"/>">
	<link rel="icon" type="image/png" href="Eventar/favicon-16x16.png" sizes="16x16" />
	<link rel="icon" type="image/png" href="Eventar/favicon-32x32.png" sizes="32x32" />
	
	<script type="text/javascript">
	  	$(document).ready(function(){
		  	// the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
		    $('.modal-trigger').leanModal();
		});	
	</script>
	<script type="text/javascript">
		$(document).ready(function(){
    	$('.scrollspy').scrollSpy();});
	</script>
</head>
<body id="navbar-body" class="scrollspy">
<div class="navbar-fixed">
<nav>
	<div class="orange lighten-1 nav-wrapper">
		<div class="container">
        	<a href=${linkTo[IndexController].index} class="brand-logo waves-effect waves-light">
        		<div class="left hide-on-med-and-down">
        			<img src="<c:url value="/img/logo.png"/>" width="30" id="appicon">
					<span style="font-size: 22px;font-weight:300;"><fmt:message key="app.name"/></span>        		
        		</div>
        	</a>
        	
		
    		 <c:choose>
        		<c:when test="${userSession.isLoggedIn()}">
        		    <a href=${linkTo[IndexController].index} class="brand-logo waves-effect waves-light">
        				<div class="left hide-on-med-and-down">
        					<img src="<c:url value="/img/logo.png"/>" width="30" id="appicon">
							<span style="font-size: 22px;font-weight:300;"><fmt:message key="app.name"/></span>        		
        				</div>
        			</a>
        		</c:when>
        		<c:otherwise>
        		    <a href=#navbar-body class="brand-logo waves-effect waves-light">
        				<div class="left hide-on-med-and-down">
        					<img src="<c:url value="/img/logo.png"/>" width="30" id="appicon">
							<span style="font-size: 22px;font-weight:300;"><fmt:message key="app.name"/></span>        		
        				</div>
        			</a>
        		</c:otherwise>
        	</c:choose>
        		
        	<ul class="right hide-on-med-and-down">
        		<c:choose>
        			<c:when test="${userSession.isLoggedIn()}">
						<li><a class="dropdown-button" data-activates="user-actions" href="#"><i class="material-icons left">perm_identity</i><fmt:message key="app.welcome"/>${userSession.currentUser.firstName}</a></li>
						<!-- <li><a class="dropdown-button" data-activates="dropdown1" href="#"><i class="material-icons">person_pin</i></a></li> -->     		
						<li><a href="${linkTo[LoginController].logout}"><i class="fa fa-sign-out"></i></a></li>
        			</c:when>
        			<c:otherwise>
		        		<li><a class="modal-trigger" href="#login-modal"><fmt:message key="app.login"/></a></li>
		            	<li><a class="modal-trigger" href="#form-modal"><fmt:message key="app.signup"/></a></li>
		            	<li><a href="#events"><fmt:message key="app.events"/></a></li>
		            	<li><a href="#contact"><fmt:message key="app.contact"/></a></li>        		
        			</c:otherwise>
        		</c:choose>
          	</ul>		
		</div>
    </div>
</nav>

<!-- Dropdown Structure -->
<ul id="user-actions" class="dropdown-content">
  <li><a class="orange-text lighten-1" href="${linkTo[UserController].show}?id=0"><fmt:message key="app.myProfile"/></a></li>
  <li><a class="orange-text lighten-1" href="${linkTo[EventController].list}"><fmt:message key="app.events"/></a></li>
  <li><a class="orange-text lighten-1" href="${linkTo[EventController].my }"><fmt:message key="event.list.my"/></a></li>
  <li><a class="orange-text lighten-1" href="${linkTo[EventController].form}"><fmt:message key="event.create.label"/></a></li>
</ul>


</div>

<c:import url="/WEB-INF/jsp/login/loginModal.jsp"></c:import>
<c:import url="/WEB-INF/jsp/user/formModal.jsp"></c:import>

</body>
</html>