<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css">
.card{
	height: 410px;
}

.card_date {
  position: absolute;
  top: 20px;
  right: 20px;
  text-align: center;
}
.actions_group{
	position: absolute; 
	top: 20px; 
	left: 20px;
}
.action_chip{
	width: 110px;
	text-align: center;
	margin: 5px;
}


.card-image img{
	width: 100%;
	height: 250px;
	margin:auto;
  	display:block;
}
</style>
</head>
<body>
<c:choose>
			<c:when test="${eventList.size()>0}">
				<c:forEach items="${eventList}" var="event">
					<div class="col s12 m12 l4">
						<div class="card">
						
					    	<div class="card-image waves-effect waves-block waves-light">
					      		<img class="activator" src="/Eventar/event/image?id=${event.id}">

					      		<div class="card_date chip">
	    							<fmt:formatDate type="date" dateStyle="medium" timeStyle="medium" value="${event.eventDate}" />
	  							</div>
					    	</div>
					    	
					    	<div class="card-content">
					      		<span class="card-title activator grey-text text-darken-4" style="line-height: 100%">${event.title}</span>
					    	</div>
					    	
					    	<div class="card-reveal">
					      		<span class="card-title grey-text text-darken-4" style="line-height: 100%">${event.title}<i class="material-icons right">close</i></span>
					      		<p>
									<c:choose>
										<c:when test="${event.description.length()>= 250}">
											${event.description.substring(0, 250)}...
										</c:when>
										<c:otherwise>
											${event.description}
										</c:otherwise>
									</c:choose>
					      		</p>
					      		
					      		<p><fmt:formatDate type="date" dateStyle="medium" timeStyle="medium" value="${event.eventDate}" /></p>
					      		
					      		<ul>
					      			<li>${event.address.streetAddress}, ${event.address.number}</li>
					      			<li>${event.address.neighborhood}</li>
					      			<li>${event.address.postalCode}</li>
					      			<li>${event.address.city} - ${event.address.state}</li>
					      		</ul>
					      		
								<div class="fixed-action-btn horizontal" >
									<a class="orange lighten-1 btn-floating btn-large red">
										<i class="mdi-navigation-menu"></i>
									</a>
									<ul>
										<li><a class="btn-floating yellow darken-1" href="${linkTo[EventController].show}?id=${event.id}"><i class="mdi-action-visibility"></i></a></li>
										
										<c:if test="${my}">
											<li><a class="btn-floating green" href="${linkTo[EventController].edit}?id=${event.id}"><i class="material-icons">mode_edit</i></a></li>
									    	<li><a class="btn-floating red" href="${linkTo[EventController].delete}?id=${event.id}"><i class="material-icons">delete</i></a></li>
										</c:if>
									</ul>
								</div>					     
     		
					    	</div>
					  	</div>			
					</div>
				</c:forEach>			
			</c:when>
			
			<c:otherwise>
			<div style="height:350px">
				<p><fmt:message key="default.noEventsRegistered"/></p>
			</div>
			</c:otherwise>
		</c:choose>
</body>
</html>