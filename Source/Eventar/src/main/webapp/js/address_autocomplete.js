/**
 * 	Consumindo webservice de CEP para completar os outros campos do formulário de endereço 
 */
$(document).ready(function(){
	function clear_address_form(){
		$("#address input").val("");
	}
			
	$("#postalCode").blur(function(){
		var postalCode = $(this).val().replace(/\D/g,"");
				
		if(postalCode != ""){
					
			var postalCodeValidate = /^[0-9]{8}$/;
					
			if(postalCodeValidate.test(postalCode)){
						
				$.getJSON("//viacep.com.br/ws/"+ postalCode +"/json/?callback=?", function(data){
					$("#address label").attr("class", "active");
					$("#streetAddress").val(data.logradouro);
					$("#city").val(data.localidade);
					$("#neighborhood").val(data.bairro);
					$("#state").val(data.uf);
					
					/*Coloca marcação no mapa (se houver)*/
					codeAddress();
				});
			}		
		}
	});
});